# final_project

# Contents of your repo and what each file does

The Jupyter Notebook called final_project.ipynb contains my final project code. It uses data from the 3 other folders in the repo, as is described in final_project.ipynb

The Jupyter Notebook called gen_random_data.ipynb contains code which randomizes model inputs to my model. It won't actually run, since I don't have my model code here, but you can look at it if you like. It is mainly included for completeness.

environment.yml is the environment file that binder uses. Lists the packages used in final_project.

README.ME is this file

The folders 

compare_num_streams
compare_scattering_phase_fn
rand_data

contain .nc files with data. How the data differ and how they were generated, if relevent, is covered in final_project.ipynb

The folder final_project_local_folder doesn't do anything, but Jupyter isn't letting me delete it, so no big deal :)
